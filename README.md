Para ejecutar el proyecto se necesita: 
1) Tener instalado Ruby
2) Instalar Sinatra (En el cmd ejecutar la linea 'gem install sinatra'
3) En la ruta del archivo LocalSinatra.rb (en el cmd) ejecutar la linea 'rackup' 
4) En la consola se podrá observar el puerto asignado (normalmente 9292)
5) Acceder a localhost:9292 (o el puerto asignado)