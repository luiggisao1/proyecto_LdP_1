
class Vertex
    attr_accessor :etiqueta , :edges, :aceptacion, :verticesEpsilon, :tieneEpsilonAnterior

    def initialize(etiqueta)
        @etiqueta = etiqueta
        @edges = Array.new()
        @verticesEpsilon = Array.new()
        @aceptacion=false
        @tieneEpsilonAnterior = false
    end
    attr_accessor :etiqueta , :edges

    def equals(object)
        if object!=nil
            return (self.etiqueta == object.etiqueta)
        end
        return false
    end

    def to_s
        return self.etiqueta
    end
end
