require './vertex.rb'
require './edge.rb'

class Grafo
	@@epsilon = "epsilon"
	@@alfabeto = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","ñ","o","p","q","r","s","t","u","v","w","x","y","z"]
	attr_accessor :vertices, :matriz, :efectivo, :pesos, :edges
	def initialize()
		@vertices = Array.new()
		@pesos=Array.new()
		@matriz = Array.new()
		@edges = Array.new()
		@efectivo=0
	end

	def addVertex (etiqueta, aceptacion)
		if (!self.contains(etiqueta))
			v = Vertex.new(etiqueta)
			vertices.push(v)
			v.aceptacion = aceptacion
			matriz[efectivo]=Array.new(27)
			matriz[efectivo][0]=Array.new
			self.addEdge(etiqueta,etiqueta,@@epsilon)
			@efectivo = @efectivo+1
		end
		
	end

	def addEdge (origen, destino, peso)

		i0=self.searchVertex(origen)
		id=self.searchVertex(destino)
		if (i0 == nil || id == nil)
			return false
		end

		vorigen = vertices[i0]
		vdestino = vertices[id]
		e = Edge.new(vorigen, vdestino, peso)


		if (!e.Yala)
			if (!pesos.include?(peso))
				pesos.push(peso)
			end
			
			indp= pesos.index(peso)
			if(peso == @@epsilon)
				comprobarEpsilonAnterior(vorigen,vdestino)
				matriz[i0][indp].push(vdestino)
				vertices[i0].verticesEpsilon.push(vdestino)
			else

				matriz[i0][indp] = vdestino
			end
			
			
			vorigen.edges.push(e)
			if (!e.isEpsilon)
				self.edges.push(e)
			end
			

		end
			
	end

	def removeEdge(origen, destino,peso)
		i0=self.searchVertex(origen)
		id=self.searchVertex(destino)
		if(i0 == -1 || id == -1)
			return false
		end

		vorigen = vertices[i0]
		vdestino = vertices[id]

		vorigen.edges.each do |e|
			if (e.destino.equals(vdestino) && e.peso == peso)
				vorigen.edges.delete(e)
				inp = pesos.index(e.peso)
				matriz[i0][inp] = nil
			end
		end
		edges.each do |e|
			if (e.origen == vorigen && (e.destino == vdestino) && e.peso == peso)
				self.edges.delete(e)
			end
		end
		
	end
	
		
	def searchVertex(elemento)
		
		if (elemento != nil)
			indice = 0
			self.vertices.each do |vertice|
				
				if (vertice.etiqueta==elemento)
					return indice
				end
				indice+=1
				
			end
		end
		return -1
	end

	def contains (elemento)
		if (elemento != nil)
			indice = 0
			self.vertices.each do |vertice|
				if (vertice.etiqueta==elemento)
					return true
				end
			
			end
		end
		return false
	end

	def borrarPeso (indice, peso)
		(0..(self.edges.size-1)).each do |i|
			if (edges[i].peso == peso && indice != i)
				return
			end
		end
		self.pesos.delete(pesos)
	end

	def borrarpesoEpsilon
		self.pesos.delete(pesos[0])
		(0..(vertices.size)-1).each do |i|
			matriz[i].delete_at(0)

			vertices[i].edges.each do |edge|
				if (edge.peso == @@epsilon)
					vertices[i].edges.delete(edge)
				end
			end
		end
	end

	def borrarVertex(vertice)
		indice = self.searchVertex(vertice.etiqueta)
		self.vertices.delete_at(indice)
		self.matriz.delete_at(indice)
	end

	def to_s
		builder = "Vertices."
		(0..(pesos.size()-1)).each do |i|
			if (i == pesos.size-1)
				builder << pesos[i]+"|"
			else
				builder << pesos[i]+"."
			end
		end

		(0..(vertices.size()-1)).each do |i|
			arrayI = matriz[i]
			vorigen = vertices[i]
			builder << vorigen.to_s+"."
			(0..(pesos.size()-1)).each do |j|
				if !(arrayI[j].instance_of?(Array))
					if (arrayI[j] == nil)
						if (j == pesos.size()-1)
							builder << 0.to_s+"|"
						else
							builder << 0.to_s+"."
						end
						
					else
						vdestino = arrayI[j]
						if (j == pesos.size()-1)
							builder << vdestino.to_s+"|"
						else
							builder << vdestino.to_s+"."
						end
						
					 	
					end
				else
					vertices = arrayI[j]
					buff = "["+vertices.join(",")+"]."
					builder << buff
				end

				
			end
		end
		return builder
	end

	def grafoADot
		if(self.vertices == 0)
			return nil
		end 
		builder = "digraph nfa {\n\t\"\" [shape=none]\n"
		if (vertices[0].edges.size() == 0)
			self.borrarVertex(vertices[0])
		end
		(self.vertices).each do |vertice|
			builder << "\t\""+vertice.etiqueta.to_s+"\" [shape="
			if(vertice.aceptacion)
				builder << "doublecircle]\n"
			else 
				builder << "circle]\n"
			end
		end 
		builder << "\n\n\t\"\" -> \""+vertices[0].to_s+"\"\n"
		(self.vertices).each do |vertice|
			(vertice.edges).each do |edge|
				if(!((edge.origen.etiqueta).to_s == (edge.destino.etiqueta).to_s && edge.peso == @@epsilon))
					builder << "\t\""+(edge.origen.etiqueta).to_s+"\" -> \""+(edge.destino.etiqueta).to_s+"\" [label=\""+(edge.peso).to_s+"\"]\n"
				end
			end
		end 
		builder << "}"
		builder
	end

	def letPesos
		listatmp = []
		(0..(self.pesos.size()-1)).each do |i|
			peso = pesos[i]
			if(peso.length == 1 || peso=="epsilon")
				listatmp << peso
				
			else
				indp = pesos.index(peso)
				self.edges.each do |edge|
					if (edge.peso == peso)
						self.edges.delete(edge)
					end
				end
				(0..(vertices.size)-1).each do |i|	
					matriz[i].delete_at(indp)
					vertices[i].edges.each do |edge|
						if(edge.peso == peso)
							vertices[i].edges.delete(edge)
						end
					end
				end

			end


		end
		pesos.replace(listatmp)
=begin
		(0..(self.pesos.size()-1)).each do |i|
			puts "ENTRE AQUIII!!"
			puts i
			peso = pesos[i]
			puts peso
			if((peso.length > 1) && (peso != @@epsilon))
				indp = pesos.index(peso)
				self.pesos.delete_at(indp)
				self.edges.each do |edge|
					if (edge.peso == peso)
						self.edges.delete(edge)
					end
				end
				(0..(vertices.size)-1).each do |i|	
					matriz[i].delete_at(indp)
					vertices[i].edges.each do |edge|
						if(edge.peso == peso)
							vertices[i].edges.delete(edge)
						end
					end
				end
			end	
		end
		puts "SALI"
=end
	end

	def comprobarEpsilonAnterior(vorigen,vdestino)
		if !(vorigen.equals(vdestino))
			vdestino.tieneEpsilonAnterior = true
		end

	end


end


